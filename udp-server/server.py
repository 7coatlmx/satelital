import socket
import json
import asyncio
import websockets
from message import Message

class error:
    def __init__(self):
        self.errorCode="server issue"
        self.errorMessage="201"


class BaseRespone():
    def __init__(self, msg=None):
        self.success = "data fetch successfully"
        self.data    = {"success": msg}
        self.error   = error()

def obj_to_dict(obj):
   return obj.__dict__


async def hello(tokens):
    async with websockets.connect('ws://localhost:8765') as websocket:
        msg = Message(tokens[0],tokens[1],tokens[2],tokens[3],tokens[4])

        bs = BaseRespone(msg)
        json_string = json.dumps(bs.__dict__, default = obj_to_dict)

        print(f"> {msg.id}")
        print(json_string)
        await websocket.send(json_string)

        greeting = await websocket.recv()
        print(f"< {greeting}")


localIP       = "127.0.0.1"
localPort     = 20001
bufferSize    = 1024
msgFromServer = "Hello UDP Client"
bytesToSend   = str.encode(msgFromServer)

# Create a datagram socket
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Bind to address and ip
UDPServerSocket.bind((localIP, localPort))

print("UDP server up and listening")

# Listen for incoming datagrams

while(True):
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    message = bytesAddressPair[0]
    address = bytesAddressPair[1]

    clientMsg = "Message from Client:{}".format(message)
    clientIP  = "Client IP Address:{}".format(address)

    print(message)
    tokens = message.decode("utf-8") .split(":")
    #print(length(tokens))
#    msg   = Message(tokens[0],tokens[1],tokens[2],tokens[3],tokens[4])

#    bs = BaseRespone(msg)
#    json_string = json.dumps(bs.__dict__, default = obj_to_dict)
    #json_string  = json.dumps(bs.__dict__,  default = obj_to_dict)
    #json_string1 = json.loads(json_string)
    
    #print(js)
    #print(json_string)
#    print(json_string)
    #print(clientIP)

    # Sending a reply to client
    asyncio.get_event_loop().run_until_complete(hello(tokens))
    UDPServerSocket.sendto(bytesToSend, address)
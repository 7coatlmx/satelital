class Message:
	_id   = None
	_date = None
	_time = None
	_lat  = None
	_lng  = None

	def __init__(self, id=None, date=None, time=None, lat=None, lng=None):
		self._id   = id
		self._date = date
		self._time = time
		self._lat  = lat
		self._lng  = lng

	@property
	def id(self):
		return self._id

	@id.setter
	def id(self, id=None):
		self._id = id

	@property
	def date(self):
		return self._date
	
	@date.setter
	def date(self, date=None):
		self._date = date

	@property
	def time(self):
		return self._time

	@time.setter
	def time(self, time=None):
		self._time = time

	@property
	def lat(self):
		return self._lat
	
	@lat.setter
	def lat(self, lat=None):
		self._lat = lat

	@property
	def lng(self):
		return self._lng
	
	@lng.setter
	def lng(self, lat=None):
		self._lng = lng
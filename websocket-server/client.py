#!/usr/bin/env python

# WS client example

import asyncio
import websockets
import json
from message import Message

class error:
    def __init__(self):
        self.errorCode="server issue"
        self.errorMessage="201"


class BaseRespone():
    def __init__(self, msg=None):
        self.success = "data fetch successfully"
        self.data    = {"success": msg}
        self.error   = error()

def obj_to_dict(obj):
   return obj.__dict__

async def hello():
    async with websockets.connect('ws://localhost:8765') as websocket:
        msg = Message(1,20190707,1500,1.2345,2.3456)

        bs = BaseRespone(msg)
        json_string = json.dumps(bs.__dict__, default = obj_to_dict)

        print(f"> {msg.id}")
        print(json_string)
        await websocket.send(json_string)

        greeting = await websocket.recv()
        print(f"< {greeting}")

asyncio.get_event_loop().run_until_complete(hello())
#!/usr/bin/env python

# WS server example
import asyncio
import websockets
import json
from message import Message

USERS = set()
def register(websocket):
    USERS.add(websocket)

async def hello(websocket, path):
    register(websocket)
    try:
        data = await websocket.recv()
        print(data)

        #d = json.loads(data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        d = json.loads(data)
        #print(msg)
        print(d)
        e = d['data']
        print(e)
        msg = e['success']
        print(msg)

        dd = json.dumps(msg)

        if USERS:       # asyncio.wait doesn't accept an empty list
            await asyncio.wait([user.send(dd) for user in USERS])
    except:
        pass

start_server = websockets.serve(hello, 'localhost', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
import React, { Component, Fragment } from 'react';
//import logo from './logo.svg';
import './App.css';
import { Map, Marker, InfoWindow, GoogleApiWrapper } from 'google-maps-react';
import Websocket from 'react-websocket';
import { Container, Row, Col, Nav, NavItem, NavLink } from 'reactstrap';

//import Post from './components/Post';
import Header from './components/Header';
//import Sider1 from './components/Sider';
//import SideCard from './components/SideCard';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locations: []
    };
  }

  handleData = (data) => {
    let result = JSON.parse(data);
    //console.log(result);
    
    //We get a new point
    const location = result;

    // We create a copy of current state
    //const locs = this.state.locations.slice();

    //Flag for check new if id exists
    let idExists = false;

    for (let n = 0; n < this.state.locations.length; n++) {
      if (this.state.locations[n]._id === location._id) {
        this.state.locations.splice(n,1,location);
        idExists = true;
        break;
      }
    }

    if (!idExists) {
      this.setState(prevState => ({
        locations: [...prevState.locations, location]
      }));    
    } else {
      this.setState(prevState => ({
        locations: [...prevState.locations]
      }));  
    }

    //this.setState ({locations: this.state.locations});
    
    //map.panTo(location);
  }

  render() {
    return (
      <div className="App">
              <Fragment>
              <Header />
              </Fragment>
            <Container fluid>
                <Row>
                    <Col>
                        <Nav vertical class="sidebar">
                        <div class="sidebar-sticky">
                            <NavItem>
                                <NavLink href="#">Link 1</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Link 2</NavLink>
                            </NavItem>
                            </div>
                        </Nav>
                    </Col>
                    <Col>
                        <Map 
                            google={this.props.google} 
                            zoom={18}
                            initialCenter={{
                                lat: 19.432608,
                                lng: -99.133209
                            }}  
                        >

                            {this.state.locations.map((location, i) => {
                                    return (
                                    <Marker
                                        key={i}
                                        position={{ lat: location._lat, lng: location._lng }}
                                    />
                                    );
                                })
                            }

                            <Websocket url='ws://localhost:8765'
                                onMessage={this.handleData.bind(this)}/>
                        </Map>
                    </Col>
                </Row>
            </Container>
      </div>
    );
  }
}

//export default App;

export default GoogleApiWrapper({
  apiKey: ('AIzaSyCHHIA0RDYkAg5cLsfkb39SRMlWFcXgOP4')
 })(App);

import React, { Component } from 'react';
import { Container, Row, Col, Nav, NavItem, NavLink } from 'reactstrap';
import { Map, Marker, InfoWindow, GoogleApiWrapper } from 'google-maps-react';
import Websocket from 'react-websocket';

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
          locations: []
        };
    }

    handleData = (data) => {
        let result = JSON.parse(data);
        //console.log(result);
        
        //We get a new point
        const location = result;
    
        // We create a copy of current state
        //const locs = this.state.locations.slice();
    
        //Flag for check new if id exists
        let idExists = false;
    
        for (let n = 0; n < this.state.locations.length; n++) {
          if (this.state.locations[n]._id === location._id) {
            this.state.locations.splice(n,1,location);
            idExists = true;
            break;
          }
        }
    
        if (!idExists) {
          this.setState(prevState => ({
            locations: [...prevState.locations, location]
          }));    
        } else {
          this.setState(prevState => ({
            locations: [...prevState.locations]
          }));  
        }
    
        //this.setState ({locations: this.state.locations});
        
        //map.panTo(location);
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col></Col>
                </Row>
                <Row>
                    <Col>
                        <Nav vertical class="sidebar">
                            <NavItem>
                                <NavLink href="#">Link 1</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="#">Link 2</NavLink>
                            </NavItem>
                        </Nav>
                    </Col>
                    <Col>
                        <Map 
                            google={this.props.google} 
                            zoom={18}
                            initialCenter={{
                                lat: 19.432608,
                                lng: -99.133209
                            }}  
                        >

                            {this.state.locations.map((location, i) => {
                                    return (
                                    <Marker
                                        key={i}
                                        position={{ lat: location._lat, lng: location._lng }}
                                    />
                                    );
                                })
                            }

                            <Websocket url='ws://localhost:8765'
                                onMessage={this.handleData.bind(this)}/>
                        </Map>
                    </Col>
                </Row>
            </Container>
        );
    }
}
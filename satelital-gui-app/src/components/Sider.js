import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';

class Sider1 extends React.Component {
    render(){
        return (
            <ListGroup width="50px">
                <ListGroupItem>Hola</ListGroupItem>
                <ListGroupItem>a</ListGroupItem>
                <ListGroupItem>todo</ListGroupItem>
                <ListGroupItem>el</ListGroupItem>
                <ListGroupItem>mundo</ListGroupItem>
            </ListGroup>
        );
    }
}

export default Sider1;